package Proiect;

import java.sql.*;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws Exception {

        Inserare ins = new Inserare();
        Scanner input = new Scanner(System.in);
        Citire cit = new Citire();
        Datornici d = new Datornici();
        Status status = new Status();
        Stergere del = new Stergere();
        AfisareClienti af = new AfisareClienti();
        int exit = 7;
        int optiune;
        do {
            System.out.println("Alegeti o optiune din meniu:");
            System.out.println("1. Afisarea clientilor.\n2. Introducerea unui client nou in baza de date\n"
                    + "3. Afisarea clientilor datori\n"
                    + "4. Actualizare status client\n"
                    + "5. Stergerea unui client din baza de date\n"
                    + "6. Afisarea tuturor datelor unui client\n"
                    + "7. Exit");
            System.out.println("Introduceti optiunea dorita: ");

            try {
                optiune = input.nextInt();
            } catch (NumberFormatException e) {
                //e.printStackTrace();
                optiune = -1;
            }
            if (optiune == 1) {
                cit.citeste();
            } else if (optiune == 2) {
                ins.insert();
            } else if (optiune == 3) {
                d.afisareDatornici();
            } else if (optiune == 4) {
                status.alegerefact();
            } else if (optiune == 5) {
                del.stergereClient();
            } else if (optiune == 6) {
                af.alegereoptiune();
            }

        } while (optiune != exit);
        System.out.println("Aplicatia se inchide...");

    }

}

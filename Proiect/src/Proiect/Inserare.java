package Proiect;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Scanner;

public class Inserare {

    boolean valid;
    int id;
    String nume;
    Double suma;

    public void insert() throws Exception {
        Class.forName("org.apache.derby.jdbc.ClientDriver");
        // System.out.println("JDBC driver loaded.");
        Connection con = DriverManager.getConnection("jdbc:derby://localhost:1527/school", "app", "app");
        // System.out.println("Connected to database!");
        Statement st = con.createStatement();
        //System.out.println("Execute query");
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Introduceti codul de identificare fiscala: ");
        do {
            valid = true;
            try {
                id = Integer.parseInt(keyboard.nextLine());
            } catch (NumberFormatException e) {
                //e.printStackTrace();
                System.out.println("Codul de identificare fiscala trebuie sa fie un numar. Reintroduceti codul:");
                valid = false;
            }
        } while (valid == false);

        System.out.println("Introduceti denumirea firmei: ");

        do {
            nume = keyboard.nextLine();
            if (nume.length() == 0) {
                System.out.println("Trebuie sa introduceti denumirea firmei:");
            } else {
                break;
            }

        } while (true);

        System.out.println("Introduceti orasul: ");
        String oras = keyboard.nextLine();
        System.out.println("Introduceti judetul: ");
        String judet = keyboard.nextLine();
        System.out.println("Introduceti adresa: ");
        String adresa = keyboard.nextLine();
        System.out.println("Introduceti telefonul: ");
        String telefon = keyboard.nextLine();
        System.out.println("Introduceti suma datorata: ");
        do {
            valid = true;
            try {
                suma = Double.parseDouble(keyboard.nextLine());
            } catch (NumberFormatException e) {
                //e.printStackTrace();
                System.out.println("Suma introdusa trebuie sa fie un numar. Reintroduceti suma:");
                valid = false;
            }
        } while (valid == false);
        String q1 = "select * from clienti WHERE id = '" + id + "'";
        ResultSet rs = st.executeQuery(q1);
        if (rs.next()) {
            System.out.println("-----Aceasta firma este deja inregistrata!-----");

        } else {

            q1 = "insert into clienti values('" + id + "', '" + nume.toLowerCase() + "', '" + oras + "', '" + judet + "', '" + adresa + "', '" + telefon + "', '" + suma.toString() + "')";
            int x = st.executeUpdate(q1);

            if (x > 0) {
                System.out.println("-------Firma a fost introdusa cu succes in baza de date!-------\n");
            } else {
                System.out.println("-------Introducerea firmei in baza de date a esuat!-------\n");
            }
        }

    }

}

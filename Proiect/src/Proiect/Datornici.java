package Proiect;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Datornici {

    public void afisareDatornici() throws Exception {
        Class.forName("org.apache.derby.jdbc.ClientDriver");
        //System.out.println("JDBC driver loaded.");
        Connection con = DriverManager.getConnection("jdbc:derby://localhost:1527/school", "app", "app");
        // System.out.println("Connected to database!");
        Statement st = con.createStatement();
        // System.out.println("Execute query");

        ResultSet rs = st.executeQuery("SELECT * FROM CLIENTI ORDER BY NUME");
        System.out.printf("%-20.20s  %-20.20s %-20.20s\n", "Denumire firma", "CIF", "Suma");
        System.out.println("---------------------------------------------------");
        while (rs.next()) {
            String id = rs.getString(1);
            String nume = rs.getString(2);
            String oras = rs.getString(3);
            String telefon = rs.getString(6);
            String suma = rs.getString(7);
            double d = Double.parseDouble(suma);
            if (d > 0) {

                System.out.printf("%-20.20s  %-20.20s %-20.20s\n", nume.toUpperCase(), id, suma);
            }
        }
        con.close();
        //System.out.println("Database connection closed.");
        System.out.println("---------------------------------------------------");
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Proiect;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Citire {

    String id, nume, oras, telefon;

    public void citeste() throws Exception {
        Class.forName("org.apache.derby.jdbc.ClientDriver");
        //System.out.println("JDBC driver loaded.");
        Connection con = DriverManager.getConnection("jdbc:derby://localhost:1527/school", "app", "app");
        // System.out.println("Connected to database!");
        Statement st = con.createStatement();
        // System.out.println("Execute query");

        ResultSet rs = st.executeQuery("SELECT * FROM CLIENTI ORDER BY NUME");
        System.out.printf("%-20.20s  %-20.20s %-20.20s  %-20.20s\n", "Denumire firma", "CIF", "Oras", "Telefon");
        //  System.out.println(" Denumire firma        CUI                 Oras                  Telefon");
        System.out.println("----------------------------------------------------------------------------");
        while (rs.next()) {
            id = rs.getString(1);
            nume = rs.getString(2);
            oras = rs.getString(3);
            telefon = rs.getString(6);

            System.out.printf("%-20.20s  %-20.20s %-20.20s  %-20.20s\n", nume.toUpperCase(), id, oras, telefon);

        }
        con.close();
        //System.out.println("Database connection closed.");
        System.out.println("----------------------------------------------------------------------------");
    }
}

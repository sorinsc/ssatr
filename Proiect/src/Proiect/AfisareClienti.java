package Proiect;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Scanner;

public class AfisareClienti {

    boolean valid;
    int id;
    String nume;

    public void alegereoptiune() {
        Scanner input = new Scanner(System.in);
        int exit = 0;
        int answer;

        do {
            System.out.println("Cautati firma dupa: 1. Denumire  |  2. CIF");
            System.out.println("Alegeti '1', '2' sau '0' pentru a reveni la meniul principal.");
            try {
                answer = input.nextInt();
            } catch (Exception e) {
                answer = 0;
            }
            if (answer == 1) {
                afisareclient_nume();
            } else if (answer == 2) {
                afisareclient_cif();
            }

        } while (answer != exit);
    }

    public void afisareclient_nume() {

        try {
            Class.forName("org.apache.derby.jdbc.ClientDriver");
            //System.out.println("JDBC driver loaded.");
            Connection con = DriverManager.getConnection("jdbc:derby://localhost:1527/school", "app", "app");
            // System.out.println("Connected to database!");
            Statement st = con.createStatement();
            // System.out.println("Execute query");
            Scanner keyboard = new Scanner(System.in);
            System.out.println("Introduceti denumirea firmei: ");

            do {
                nume = keyboard.nextLine();
                if (nume.length() == 0) {
                    System.out.println("Trebuie sa introduceti denumirea firmei:");
                } else {
                    break;
                }

            } while (true);
            String q1 = "select * from clienti WHERE nume = '" + nume.toLowerCase() + "'";
            ResultSet rs = st.executeQuery(q1);
            if (rs.next()) {
                System.out.printf("%-20.20s  %-20.20s %-20.20s  %-20.20s %-30.30s  %-20.20s %-20.20s\n", "Denumire firma", "CUI", "Oras", "Judet", "Adresa", "Telefon", "Suma");

                System.out.println("-------------------------------------------------------------------------------------------------------------------------------------------------");
                System.out.printf("%-20.20s  %-20.20s %-20.20s  %-20.20s %-30.30s  %-20.20s %-20.20s\n", rs.getString(2).toUpperCase(), rs.getString(1), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7));
                System.out.println("-------------------------------------------------------------------------------------------------------------------------------------------------");
            } else {
                System.out.println("-------Firma nu exista in baza de date!-------\n");
            }
            while (rs.next()) {

                System.out.printf("%-20.20s  %-20.20s %-20.20s  %-20.20s %-30.30s  %-20.20s %-20.20s\n", rs.getString(2).toUpperCase(), rs.getString(1), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7));
                System.out.println("-------------------------------------------------------------------------------------------------------------------------------------------------");

            }

            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void afisareclient_cif() {

        try {
            Class.forName("org.apache.derby.jdbc.ClientDriver");
            //System.out.println("JDBC driver loaded.");
            Connection con = DriverManager.getConnection("jdbc:derby://localhost:1527/school", "app", "app");
            // System.out.println("Connected to database!");
            Statement st = con.createStatement();
            // System.out.println("Execute query");
            Scanner keyboard = new Scanner(System.in);
            System.out.println("Introduceti codul de identificare fiscala: ");

            do {
                valid = true;
                try {
                    id = Integer.parseInt(keyboard.nextLine());
                } catch (NumberFormatException e) {
                    //e.printStackTrace();
                    System.out.println("Codul de identificare fiscala trebuie sa fie un numar. Reintroduceti codul:");
                    valid = false;
                }
            } while (valid == false);

            String q1 = "select * from clienti WHERE id = '" + id + "'";
            ResultSet rs = st.executeQuery(q1);
            if (rs.next()) {
                System.out.printf("%-20.20s  %-20.20s %-20.20s  %-20.20s %-30.30s  %-20.20s %-20.20s\n", "Denumire firma", "CUI", "Oras", "Judet", "Adresa", "Telefon", "Suma");
                // System.out.println("Denumire firma        CUI                 Oras                  Judet                 Adresa               Telefon            Suma");
                System.out.println("-------------------------------------------------------------------------------------------------------------------------------------------------");
                System.out.printf("%-20.20s  %-20.20s %-20.20s  %-20.20s %-30.30s  %-20.20s %-20.20s\n", rs.getString(2).toUpperCase(), rs.getString(1), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7));
                System.out.println("-------------------------------------------------------------------------------------------------------------------------------------------------");

            } else {
                System.out.println("-------Firma nu exista in baza de date!-------\n");
            }
            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}

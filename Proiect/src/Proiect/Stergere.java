package Proiect;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.Scanner;

public class Stergere {

    int id;
    boolean valid;

    public void stergereClient() throws Exception {
        Class.forName("org.apache.derby.jdbc.ClientDriver");
        // System.out.println("JDBC driver loaded.");
        Connection con = DriverManager.getConnection("jdbc:derby://localhost:1527/school", "app", "app");
        // System.out.println("Connected to database!");
        Statement st = con.createStatement();
        // System.out.println("Execute query");

        Scanner keyboard = new Scanner(System.in);
        System.out.println("Introduceti codul de identificare fiscala: ");

        do {
            valid = true;
            try {
                id = Integer.parseInt(keyboard.nextLine());
            } catch (NumberFormatException e) {
                //e.printStackTrace();
                System.out.println("Codul de identificare fiscala trebuie sa fie un numar. Reintroduceti codul:");
                valid = false;
            }
        } while (valid == false);
        String q1 = "DELETE from clienti WHERE id = '" + id + "'";

        int x = st.executeUpdate(q1);

        if (x > 0) {
            System.out.println("-------Firma a fost stearsa cu succes!-------\n");
        } else {
            System.out.println("-------A aparut o eroare! Verificati datele introduse!-------\n");
        }

        con.close();
    }
}

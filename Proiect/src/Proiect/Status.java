package Proiect;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Scanner;

public class Status {

    Boolean valid;
    int id;
    double suma;

    public void alegerefact() {
        Scanner input = new Scanner(System.in);
        int exit = 0;
        int answer;

        do {
            System.out.println("1. S-a emis o noua factura  |  2. S-a achitat o factura veche");
            System.out.println("Alegeti '1', '2' sau '0' pentru a reveni la meniul principal.");
            try {
                answer = input.nextInt();
            } catch (Exception e) {
                answer = 0;
            }
            if (answer == 1) {
                actualizareemitere();
            } else if (answer == 2) {
                actualizareplata();
            }

        } while (answer != exit);
    }

    public void actualizareplata() {
        String s = "";
        try {
            Class.forName("org.apache.derby.jdbc.ClientDriver");
            // System.out.println("JDBC driver loaded.");
            Connection con = DriverManager.getConnection("jdbc:derby://localhost:1527/school", "app", "app");
            // System.out.println("Connected to database!");
            Statement st = con.createStatement();
            //System.out.println("Execute query");
            Scanner keyboard = new Scanner(System.in);
            System.out.println("Introduceti codul de identificare fiscala: ");
            do {
                valid = true;
                try {
                    id = Integer.parseInt(keyboard.nextLine());
                } catch (NumberFormatException e) {
                    //e.printStackTrace();
                    System.out.println("Codul de identificare fiscala trebuie sa fie un numar. Reintroduceti codul:");
                    valid = false;
                }
            } while (valid == false);
            System.out.println("Introduceti suma achitata: ");
            do {
                valid = true;
                try {
                    suma = Double.parseDouble(keyboard.nextLine());
                } catch (NumberFormatException e) {
                    //e.printStackTrace();
                    System.out.println("Suma introdusa trebuie sa fie un numar. Reintroduceti suma:");
                    valid = false;
                }
            } while (valid == false);
            ResultSet rs = st.executeQuery("SELECT * FROM CLIENTI");

            while (rs.next()) {

                String id1 = rs.getString(1);
                int id2 = Integer.parseInt(id1);
                if (id == id2) {
                    String nume = rs.getString(2);
                    String oras = rs.getString(3);
                    String telefon = rs.getString(6);
                    String suma1 = rs.getString(7);
                    double d = Double.parseDouble(suma1);
                    double d1 = d - suma;
                    s = String.valueOf(d1);

                }
            }
            String q1 = "UPDATE clienti set suma = '" + s + "' WHERE id = '" + id + "'";
            int x = st.executeUpdate(q1);

            if (x > 0) {
                System.out.println("Suma a fost actualizata cu succes!\n");
            } else {
                System.out.println("A avut loc o eroare! Verificati datele introduse!\n");
            }
            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public void actualizareemitere() {
        String s = "";
        try {
            Class.forName("org.apache.derby.jdbc.ClientDriver");
            // System.out.println("JDBC driver loaded.");
            Connection con = DriverManager.getConnection("jdbc:derby://localhost:1527/school", "app", "app");
            // System.out.println("Connected to database!");
            Statement st = con.createStatement();
            //System.out.println("Execute query");
            Scanner keyboard = new Scanner(System.in);
            System.out.println("Introduceti codul de identificare fiscala: ");
            do {
                valid = true;
                try {
                    id = Integer.parseInt(keyboard.nextLine());
                } catch (NumberFormatException e) {
                    //e.printStackTrace();
                    System.out.println("Codul de identificare fiscala trebuie sa fie un numar. Reintroduceti codul:");
                    valid = false;
                }
            } while (valid == false);
            System.out.println("Introduceti valoarea facturii emise: ");
            do {
                valid = true;
                try {
                    suma = Double.parseDouble(keyboard.nextLine());
                } catch (NumberFormatException e) {
                    //e.printStackTrace();
                    System.out.println("Suma introdusa trebuie sa fie un numar. Reintroduceti suma:");
                    valid = false;
                }
            } while (valid == false);
            ResultSet rs = st.executeQuery("SELECT * FROM CLIENTI");

            while (rs.next()) {

                String id1 = rs.getString(1);
                int id2 = Integer.parseInt(id1);
                if (id == id2) {
                    String nume = rs.getString(2);
                    String oras = rs.getString(3);
                    String telefon = rs.getString(6);
                    String suma1 = rs.getString(7);
                    double d = Double.parseDouble(suma1);
                    double d1 = d + suma;
                    s = String.valueOf(d1);

                }
            }
            String q1 = "UPDATE clienti set suma = '" + s + "' WHERE id = '" + id + "'";
            int x = st.executeUpdate(q1);

            if (x > 0) {
                System.out.println("Suma a fost actualizata cu succes!\n");
            } else {
                System.out.println("A avut loc o eroare! Verificati datele introduse!\n");
            }
            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

}

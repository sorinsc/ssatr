/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laborator1;

/**
 *
 * @author so
 */
public class Lift {
    
    private int etaj;

    Lift(int etaj) {
        this.etaj = etaj;
    }

    void urca() {
        etaj++;

        if (etaj > 5) {
            System.out.println("Liftul a ajuns la ultimul etaj");
        } else {
            System.out.println("Liftul la etajul: " + etaj);
        }

    }

    void coboara() {
        etaj++;
        System.out.println("Liftul a ajuns la ultimul etaj");
    }

    int getEtaj() {
        return etaj;
    }
    
}

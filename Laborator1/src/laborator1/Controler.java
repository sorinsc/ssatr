
package laborator1;


public class Controler {
    
    private Lift l;

    Controler(Lift l) {
        this.l = l;
    }

    /**
     * + * Metoda va deplasa liftul la etajul dorit (urca sau coboara) in
     * functie de + * etajul curent, dupa care va afisa mesajul 'Liftul a ajuns.
     * Se deschid usile.' + * @param etaj Etaj destinatie. +
     */
    void cheamaLift(int etaj) {
        while (etaj != l.getEtaj()) {

            if (etaj >= l.getEtaj()) {
                l.urca();
            } else {
                l.coboara();
            }

        }
        System.out.println("Liftul a ajuns");

    }
    
}

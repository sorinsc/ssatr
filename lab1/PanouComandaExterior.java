package hello;

public class PanouComandaExterior {

    private int etajCurent;
    private Controler c;

    public PanouComandaExterior(Controler c, int etajCurent) {
        this.etajCurent = etajCurent;
        this.c = c;
    }

    void cheamaLift() {
        c.cheamaLift(etajCurent);

    }
}

package hello;

public class lift {

    private int etaj;

    lift(int etaj) {
        this.etaj = etaj;
    }

    void urca() {
        etaj++;

        if (etaj > 5) {
            System.out.println("Liftul a ajuns la ultimul etaj");
        } else {
            System.out.println("Liftul la etajul: " + etaj);
        }

    }

    void coboara() {
        etaj++;
        System.out.println("Liftul a ajuns la ultimul etaj");
    }

    int getEtaj() {
        return etaj;
    }
}
